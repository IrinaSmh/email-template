/*!
 * \file test_csv.cpp
 * \brief Файл с реализацией класса Test_Csv
 */
#include "test_csv.h"

typedef QMap<int, QStringList> result;
Q_DECLARE_METATYPE(Csv)
Q_DECLARE_METATYPE(result)

Test_Csv::Test_Csv()
{

}

Test_Csv::~Test_Csv()
{

}

void Test_Csv::processData(){
    QFETCH(result, expected);
    QFETCH(result, real);
    QFETCH(bool, isExpectedException);

    if(isExpectedException){
        QVERIFY2(exc1==realexc1||exc2==realexc2, "Test was failed");
    } else  QVERIFY2(expected==real, "Test was failed");
}

void Test_Csv::processData_data(){

    QTest::addColumn<result>("expected");
    QTest::addColumn<result>("real");
    QTest::addColumn<bool>( "isExpectedException" );

    bool exception = true;
    bool no_exception = false;

    Csv entity;

    //Тест 1: Типовой тест
    QStringList data;
    data.append("name;time;place;author");
    data.append("Вася;12:00;улица Сталеваров, дом 15;Валентин");
    data.append("Степан;10:30;площадь Ленина;Валентин");
    data.append("Григорий;14:00;торговый центр «Акварель»;Тимофей");

    entity.setNumberOfTemplates(3);
    entity.setData(data);

    entity.processData();
    result real = entity.getSeparatedData();

    result expected;
    expected.insert(0, {"name", "time","place","author"});
    expected.insert(1, {"Вася", "12:00","улица Сталеваров, дом 15","Валентин"});
    expected.insert(2, {"Степан", "10:30","площадь Ленина","Валентин"});
    expected.insert(3, {"Григорий", "14:00","торговый центр «Акварель»","Тимофей"});

    QTest::newRow("Type test") << expected << real << no_exception;

    //Тест 2: Отсутствие данных
    data.clear();
    real.clear();
    expected.clear();

    data.append("name;time;place;author");
    entity.setNumberOfTemplates(0);
    entity.setData(data);

    entity.processData();
    real = entity.getSeparatedData();
    expected.insert(0, {"name", "time","place","author"});

    QTest::newRow("Empty data") << expected << real << no_exception;

    //Тест 3: Отсутствие разделителя «;» в строке с названиями меток
    data.clear();
    real.clear();
    expected.clear();

    data.append("nametimeplaceauthor");
    data.append("Вася;12:00;улица Сталеваров, дом 15;Валентин");
    data.append("Степан;10:30;площадь Ленина;Валентин");
    data.append("Григорий;14:00;торговый центр «Акварель»;Тимофей");

    entity.setNumberOfTemplates(3);
    entity.setData(data);
try{
    entity.processData();
    }catch(QString e){
        realexc1 = e;
    }
    QTest::newRow("No separators in marks name string") << expected << real << exception;

    //Тест 4: Отсутствие разделителя “;” в строке 2
    data.clear();
    data.append("name;time;place;author");
    data.append("Вася12:00улица Сталеваров, дом 15Валентин");
    data.append("Степан;10:30;площадь Ленина;Валентин");
    data.append("Григорий;14:00;торговый центр «Акварель»;Тимофей");

    entity.setNumberOfTemplates(3);
    entity.setData(data);

    try{
        entity.processData();
        }catch(QString e){
            realexc2 = e;
        }
    QTest::newRow("No separators in second string") << expected << real << exception;

    //Тест 5: Экранированный разделитель
    data.clear();
    data.append("name;time;place;author");
    data.append("Вася\\;Василий;12:00;улица Сталеваров, дом 15;Валентин");
    data.append("Степан;10:30;площадь Ленина;Валентин");
    data.append("Григорий;14:00;торговый центр «Акварель»;Тимофей");

    entity.setNumberOfTemplates(3);
    entity.setData(data);

    entity.processData();
    real = entity.getSeparatedData();
    expected.insert(0, {"name", "time","place","author"});
    expected.insert(1, {"Вася\\;Василий", "12:00","улица Сталеваров, дом 15","Валентин"});
    expected.insert(2, {"Степан", "10:30","площадь Ленина","Валентин"});
    expected.insert(3, {"Григорий", "14:00","торговый центр «Акварель»","Тимофей"});

    QTest::newRow("Escaped separator") << expected << real << no_exception;

}


