/*!
 * \file main.cpp
 * \brief Файл с реализацией главной функции
*/

 /*!
 * \mainpage Генерация текстов писем по шаблону
 * <p> Программа предназначена для организации тренажеров по повышению понимания студентами младших курсов основ программирования
 * <p> Разработка проводится на основании задания на курсовой проект по дисциплине «Качество и надёжность программного обеспечения».
 * <p> Программа генерирует тексты писем на основе шаблона с метками в формате «.txt» и входных данных в формате «.csv».
 */
#include <QCoreApplication>
#include "template.h"
#include "csv.h"
#include "QTextStream"
#include "QDebug"
#include "QFile"
#include "QTextStream"
#include "QTextCodec"
#include "windows.h"
#include "test_csv.h"
#include "test_template.h"
#include <iostream>
#include <cstdlib>

int main(int argc, char *argv[])
{
    QString firstArg = QString(argv[1]);
    setlocale(LC_ALL,"Russian");
if(argc==2&&firstArg=="-test"){
    Test_Csv test_csv;
    QTest::qExec(&test_csv);

    Test_Template test_template;
    QTest::qExec(&test_template);

    getchar();
} else{
        Template temp;
        Csv data;
        try{
                temp.readTemplate(argv[1]);
                data.readData(argv[2]);
                temp.generateEmails(data);
                temp.uploadToFiles(argv[3]);
            } catch(QString exception){
                std::cout << exception.toUtf8().constData() << std::endl << std::endl;
            }

}


    return 0;
}
