/*!
 * \file template.h
 * \brief Заголовочный файл с описанием класса Template
 */
#ifndef TEMPLATE_H
#define TEMPLATE_H
#include "QList"
#include "csv.h"

/*!
 * \class Template
 * \brief Класс для чтения и обработки шаблона письма
 */
class Template
{
public:
    /// Конструктор класса Template
    Template();

    /*!
     * \brief Метод set для поля с именами меток
     */
    void setMarksName(QStringList names);
    /*!
     * \brief Метод get для поля с именами меток
     */
    QStringList getMarksName();

    /*!
     * \brief Метод set для поля с текстом шаблона
     */
    void setTextTemplate(QStringList text);
    /*!
     * \brief Метод get для поля с текстом шаблона
     */
    QStringList getTextTemplate();

    /*!
     * \brief Метод set для поля с сгенерированными письмами
     */
    void setEmails(QList<QStringList> emails);
    /*!
     * \brief Метод get для поля с сгенерированными письмами
     */
    QList<QStringList> getEmails();

    /*!
     * \brief Метод для поиска меток в шаблоне и сохранения их в объект
     */
    void getMarksFromText();

    /*!
     * \brief Метод для чтения файла с текстом шаблона и заполнения объекта Template
     * \param [in] name Имя файла с текстом шаблона
     */
    void readTemplate(QString name);

    /*!
     * \brief Метод для генерации новых писем на основе шаблона
     * \param [in] data Объект Csv с входными данными
     */
    void generateEmails(Csv data);

    /*!
     * \brief Метод для записи сгенерированных писем в текстовые файлы
     * \param [in] pathName Путь до папки, в которой сохранятся сгенерированные письма
     */
    void uploadToFiles(QString pathName);

private:
    QStringList marksName; ///< имена меток
    QStringList textTemplate; ///< текст с шаблоном письма
    QList<QStringList> emails; ///< сгенерированные письма

};

#endif // TEMPLATE_H
