/*!
 * \file test_template.cpp
 * \brief Файл с реализацией класса Test_Template
 */
#include "test_template.h"

typedef QStringList list;
typedef  QList<QStringList> letters;
Q_DECLARE_METATYPE(list)
Q_DECLARE_METATYPE(letters)
Q_DECLARE_METATYPE(Template)
Q_DECLARE_METATYPE(Csv)

Test_Template::Test_Template()
{

}

Test_Template::~Test_Template(){

}

void Test_Template::getMarksFromText(){
    QFETCH(list, expected);
    QFETCH(list, real);
    QFETCH(bool, isExpectedException);
    QFETCH(bool, exceptionsIsEqual);

    if(isExpectedException){
        QVERIFY2(exceptionsIsEqual, "Test was failed");
    } else  QVERIFY2(expected==real, "Test was failed");
}

void Test_Template::getMarksFromText_data(){
    QTest::addColumn<list>("expected");
    QTest::addColumn<list>("real");
    QTest::addColumn<bool>( "isExpectedException");
    QTest::addColumn<bool>( "exceptionsIsEqual");

    Template entity;
    //Тест 1: Типовой тест
    QStringList data = {"Уважаемый #NAME#! Просьба прибыть завтра в #TIME# на место #PLACE#.",
                        "   С уважением, #AUTHOR#."};
    QStringList expected = {"NAME", "TIME", "PLACE", "AUTHOR"};
    entity.setTextTemplate(data);
    entity.getMarksFromText();
    QStringList real = entity.getMarksName();

    QTest::newRow("Type test") << expected << real << no_exception << false;

    //Тест 2: Метка содержит прописные латинские буквы
    expected.clear();
    real.clear();
    data.clear();

    data.append("Уважаемый #Name#! Просьба прибыть завтра в #TIME# на место #PLACE#.");
    data.append("   С уважением, #AUTHOR#.");
    entity.setTextTemplate(data);
    try {
        entity.getMarksFromText();
    } catch (QString e) {
        realException = e;
    }
    expectedException = "Метка \"Name\" введена неверно.";
    QTest::newRow("Mark with lowercase letters") << expected << real << exception << (expectedException==realException);

    //Тест 3: Метка содержит русские буквы
    data.clear();
    data.append("Уважаемый #ИМЯ#! Просьба прибыть завтра в #TIME# на место #PLACE#.");
    data.append("   С уважением, #AUTHOR#.");
    entity.setTextTemplate(data);
    try {
        entity.getMarksFromText();
    } catch (QString e) {
        realException = e;
    }
    expectedException = "Метка \"ИМЯ\" введена неверно.";
    QTest::newRow("Mark with russian letters") << expected << real << exception << (expectedException==realException);

    //Тест 4: Метка содержит пробел
    data.clear();
    data.append("Уважаемый #NAME #! Просьба прибыть завтра в #TIME# на место #PLACE#.");
    data.append("   С уважением, #AUTHOR#.");
    entity.setTextTemplate(data);
    try {
        entity.getMarksFromText();
    } catch (QString e) {
        realException = e;
    }
    expectedException = "Метка \"NAME \" введена неверно.";
    QTest::newRow("Mark with whitespace") << expected << real << exception << (expectedException==realException);

    //Тест 5: Метка содержит цифру
    data.clear();
    data.append("Уважаемый #NAME1#! Просьба прибыть завтра в #TIME# на место #PLACE#.");
    data.append("   С уважением, #AUTHOR#.");
    entity.setTextTemplate(data);
    try {
        entity.getMarksFromText();
    } catch (QString e) {
        realException = e;
    }
    expectedException = "Метка \"NAME1\" введена неверно.";
    QTest::newRow("Mark with number") << expected << real << exception << (expectedException==realException);

    //Тест 6: Метка содержит символ «,»
    data.clear();
    data.append("Уважаемый #NAME,#! Просьба прибыть завтра в #TIME# на место #PLACE#.");
    data.append("   С уважением, #AUTHOR#.");
    entity.setTextTemplate(data);
    try {
        entity.getMarksFromText();
    } catch (QString e) {
        realException = e;
    }
    expectedException = "Метка \"NAME,\" введена неверно.";
    QTest::newRow("Mark with symbol \",\"") << expected << real << exception << (expectedException==realException);

    //Тест 7: Экранирование символа «#»
    data.clear();
    data.append("Уважаемый #NAME#! Просьба прибыть завтра в #TIME# на место #PLACE#.");
    data.append("   \\#С уважением, #AUTHOR#.");
    entity.setTextTemplate(data);

    expected.append("NAME");
    expected.append("TIME");
    expected.append("PLACE");
    expected.append("AUTHOR");

    entity.getMarksFromText();

    real = entity.getMarksName();

    QTest::newRow("Mark with symbols \"\\#\"") << expected << real << no_exception << false;

    //Тест 8: Ошибка в нескольких метках
    data.clear();
    data.append("Уважаемый #Name#! Просьба прибыть завтра в #TIME# на место #PLACE2#.");
    data.append("   С уважением, #AUTHOR #.");
    entity.setTextTemplate(data);
    try {
        entity.getMarksFromText();
    } catch (QString e) {
        realException = e;
    }
    expectedException = "Метки \"Name\", \"PLACE2\", \"AUTHOR \", введены неверно.";
    QTest::newRow("Several errors in marks") << expected << real << exception << (expectedException==realException);
}

void Test_Template::generateEmails(){
    QFETCH(letters, expected);
    QFETCH(letters, real);
    QFETCH(bool, isExpectedException);
    QFETCH(bool, exceptionsIsEqual);

    if(isExpectedException){
        QVERIFY2(exceptionsIsEqual, "Test was failed");
    } else  QVERIFY2(expected==real, "Test was failed");
}

void Test_Template::generateEmails_data(){
    QTest::addColumn<letters>("expected");
    QTest::addColumn<letters>("real");
    QTest::addColumn<bool>( "isExpectedException");
    QTest::addColumn<bool>( "exceptionsIsEqual");

    Csv csv;
    Template entity;
    letters realLetters, expectedLetters;
    //Тест 1: Типовой тест
    expectedLetters.append({"Уважаемый Вася! Просьба прибыть завтра в 12:00 на место улица Сталеваров, дом 15.", "   С уважением, Валентин."});
    expectedLetters.append({"Уважаемый Степан! Просьба прибыть завтра в 10:30 на место площадь Ленина.", "   С уважением, Валентин."});
    expectedLetters.append({"Уважаемый Григорий! Просьба прибыть завтра в 14:00 на место торговый центр «Акварель».", "   С уважением, Тимофей."});
    QStringList dataForCsv;
    dataForCsv.append("name;time;place;author");
    dataForCsv.append("Вася;12:00;улица Сталеваров, дом 15;Валентин");
    dataForCsv.append("Степан;10:30;площадь Ленина;Валентин");
    dataForCsv.append("Григорий;14:00;торговый центр «Акварель»;Тимофей");

    csv.setNumberOfTemplates(3);
    csv.setData(dataForCsv);
    csv.processData();

    QStringList dataForLetters = {"Уважаемый #NAME#! Просьба прибыть завтра в #TIME# на место #PLACE#.",
                        "   С уважением, #AUTHOR#."};
    entity.setTextTemplate(dataForLetters);
    entity.getMarksFromText();
    entity.generateEmails(csv);

    realLetters = entity.getEmails();
    QTest::newRow("Type test") << expectedLetters << realLetters << no_exception <<false;

    //Тест 2: Нехватка данных в строке 2
    dataForCsv.clear();
    dataForCsv.append("name;time;place;author");
    dataForCsv.append("12:00;улица Сталеваров, дом 15;Валентин");
    dataForCsv.append("Степан;10:30;площадь Ленина;Валентин");
    dataForCsv.append("Григорий;14:00;торговый центр «Акварель»;Тимофей");
    csv.setData(dataForCsv);
    csv.processData();

    try {
        entity.generateEmails(csv);
    } catch (QString e) {
        realException = e;
    }
    QTest::newRow("Lack of data in second string") << expectedLetters << realLetters << exception << (realException==exceptionForLetter);

    //Тест 3: Данных больше, чем меток в шаблоне
    dataForCsv.clear();
    dataForCsv.append("name;time;place;author");
    dataForCsv.append("Вася;12:00;улица Сталеваров, дом 15;Валентин");
    dataForCsv.append("Степан;10:30;площадь Ленина;Валентин");
    dataForCsv.append("Григорий;14:00;торговый центр «Акварель»;Тимофей");
    csv.setData(dataForCsv);
    csv.processData();

    dataForLetters.clear();
    dataForLetters.append("Привет, #NAME#!");
    entity.setTextTemplate(dataForLetters);
    entity.getMarksFromText();
    entity.generateEmails(csv);

    realLetters.clear();
    realLetters = entity.getEmails();

    expectedLetters.clear();
    expectedLetters.append({"Привет, Вася!"});
    expectedLetters.append({"Привет, Степан!"});
    expectedLetters.append({"Привет, Григорий!"});

    QTest::newRow("There is more data than in the template") << expectedLetters << realLetters << no_exception <<false;

    //Тест 4: Данных меньше, чем меток в шаблоне
    dataForCsv.clear();
    dataForCsv.append("name;time;place");
    dataForCsv.append("Вася;12:00;улица Сталеваров, дом 15");
    dataForCsv.append("Степан;10:30;площадь Ленина");
    dataForCsv.append("Григорий;14:00;торговый центр «Акварель»");
    csv.setData(dataForCsv);
    csv.processData();

    dataForLetters.clear();
    dataForLetters.append("Уважаемый #NAME#! Просьба прибыть завтра в #TIME# на место #PLACE#.");
    dataForLetters.append("   С уважением, #AUTHOR#.");
    entity.setTextTemplate(dataForLetters);
    entity.getMarksFromText();

    try {
          entity.generateEmails(csv);
    } catch (QString e) {
        realException = e;
    }
     QTest::newRow("Lack of data in second string") << expectedLetters << realLetters << exception << (realException==exceptionForLetter);
}
