/*!
 * \file csv.cpp
 * \brief Файл с реализацией класса Csv
 */
#include "csv.h"
#include"QFile"

Csv::Csv()
{

}

void Csv::setNumberOfTemplates(int number){
    this->numberOfTemplates = number;
}

int Csv::getNumberOfTemplates(){
    return this->numberOfTemplates;
}

void Csv::setSeparatedData(QMap<int, QStringList> sepData){
    this->separatedData = sepData;
}
QMap<int, QStringList> Csv::getSeparatedData(){
    return this->separatedData;
}

void Csv::setData(QStringList data){
    this->data = data;
}
QStringList Csv::getData(){
    return this->data;
}

void Csv::processData(){
    QStringList input = this->getData();
    QList<QStringList> result;
    QMap<int, QStringList> res;
    QStringList data;//выделенные данные из одной строки
    QRegExp reg("([^;]+)");
    QString oneWord;

    for(int i = 0; i < input.size(); i++){
        if(!input[i].contains(';')){
            QString ecxeption = "Ошибка заполнения входных данных в строке " + QString::number(i+1) + ".";
            throw(ecxeption);
        }
        int pos = 0;
        while((pos = reg.indexIn(input[i], pos)) != -1){
            //выделяем одно слово
            oneWord = reg.cap(1);
                 //если слово оканчивается на символ /
                if(oneWord.endsWith('\\')){
                    //выделяем следующее слово
                    pos += reg.matchedLength();
                    if((pos = reg.indexIn(input[i], pos)) != -1){
                                QString secondWord = reg.cap(1);
                                    //добавляем к предыдущему слову символ ; и следующее слово
                                    oneWord.append(';');
                                    oneWord.append(secondWord);
                         }
            }
            data.append(oneWord);
            pos += reg.matchedLength();

        }

        res.insert(i, data);
        data.clear();
    }
    this->setSeparatedData(res);
}

void Csv::readData(QString name){
     QFile file(name);
     QList<QString> text;
    if(file.exists()&&file.open(QIODevice::ReadOnly)){
        //читаем построчно
       while(!file.atEnd()){
           text.append(file.readLine());
       }
       //убираем символ перевода строки
       for(int i = 0; i < text.size()-1; i++){
            text[i].remove(text[i].length()-2, 2);
       }
       if(text.last().back()=='\n'||text.last().back()=='\r'){
           text.last().remove('\r');
           text.last().remove('\n');
       }
       file.close();
       this->setData(text);
       this->setNumberOfTemplates(text.size() - 1);
       this->processData();
    }
    else{
        QString ecxeption = "File with data not exist";
        throw(ecxeption);
    }
}


