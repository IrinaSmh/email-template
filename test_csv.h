/*!
 * \file test_csv.h
 * \brief Заголовочный файл с описанием класса Test_Csv
 */
#ifndef TEST_CSV_H
#define TEST_CSV_H

#include <QObject>
#include <QtTest/QtTest>
#include "csv.h"

/*!
 * \class Test_Csv
 * \brief Класс для тестирования методов класса Csv
 */
class Test_Csv : public QObject
{
    Q_OBJECT
public:
    /// Конструктор класса Test_Csv
    Test_Csv();

    /// Деструктор класса Test_Csv
    ~Test_Csv();

    QString exc1 = "Ошибка заполнения входных данных в строке 1.";
    QString exc2 = "Ошибка заполнения входных данных в строке 2.";
    QString realexc1;
    QString realexc2;

private slots:
    /// Метод тестирования метода processData класса Csv
    void processData();

    /// Метод подготовки данных для тестировния метода processData класса Csv
    void processData_data();
};
#endif // TEST_CSV_H
