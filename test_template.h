/*!
 * \file test_template.h
 * \brief Заголовочный файл с описанием класса Test_Template
 */
#ifndef TEST_TEMPLATE_H
#define TEST_TEMPLATE_H

#include <QObject>
#include <QtTest/QtTest>
#include "template.h"
#include "csv.h"

/*!
 * \class Test_Template
 * \brief Класс для тестирования методов класса Template
 */
class Test_Template : public QObject
{
    Q_OBJECT
public:
    /// Конструктор класса Test_Template
    Test_Template();

      /// Деструктор класса Test_Template
    ~Test_Template();

    QString realException;
    QString expectedException;
    QString exceptionForLetter = "Недостаточно данных для заполнения.";

    bool exception = true;
    bool no_exception = false;

private slots:
    /// Метод тестирования метода getMarksFromText класса Template
    void getMarksFromText();

    /// Метод подготовки данных для тестировния метода getMarksFromText класса Template
    void getMarksFromText_data();

    /// Метод тестирования метода generateEmails класса Template
    void generateEmails();

    /// Метод подготовки данных для тестировния метода generateEmails класса Template
    void generateEmails_data();
};

#endif // TEST_TEMPLATE_H
