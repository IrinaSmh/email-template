/*!
  \file template.cpp
  \brief Файл с реализацией класса Template
 */
#include "template.h"
#include "QFile"
#include "QTextStream"

Template::Template()
{

}

void Template::setMarksName(QStringList names){
    this->marksName = names;
}

QStringList Template::getMarksName(){
    return this->marksName;
}

void Template::setTextTemplate(QStringList text){
    this->textTemplate = text;
}

QStringList Template::getTextTemplate(){
    return this->textTemplate;
}

void Template::setEmails(QList<QStringList> emails){
    this->emails = emails;
}

QList<QStringList> Template::getEmails(){
    return this->emails;
}

void Template::getMarksFromText(){
    QStringList marks;
    QStringList testMarks;
    QStringList wrongMarks;
    QStringList textTemplate = this->getTextTemplate();
    QRegExp reg("[^\\\\]#([A-Z]+)#");
    QRegExp reg1("[^\\\\]#(.+)#");
    reg1.setMinimal(true);

    for(int i = 0; i < textTemplate.size(); i++){
        int pos = 0;
        while((pos = reg.indexIn(textTemplate[i], pos)) != -1){
            marks.append(reg.cap(1));
            pos += reg.matchedLength();
        }
     }

    //ищем все метки, правильные и нет метки
    for(int i = 0; i < textTemplate.size(); i++){
        int pos = 0;
        while((pos = reg1.indexIn(textTemplate[i], pos)) != -1){
            testMarks.append(reg1.cap(1));
            pos += reg1.matchedLength();
        }
     }

    //если количество правильных меток и всех меток не совпадает
    if(testMarks.size()!=marks.size()){
        bool equalsMarks = false;
        //ищем каждую метку в списке правильных
        for(int i = 0; i < testMarks.size(); i++){
            if(marks.contains(testMarks[i])) equalsMarks = true;
            if(!equalsMarks) wrongMarks.append(testMarks[i]);
             equalsMarks = false;
        }
    }

    if(!wrongMarks.isEmpty()){
        if(wrongMarks.size()>1){
            QString error = "Метки ";
        for(int i = 0; i < wrongMarks.size(); i++){
             error.append("\""+wrongMarks[i] + "\", ");
             }
            error.append("введены неверно.");
            throw(error);
        }
        else {
             QString ecxeption = "Метка \"" + wrongMarks[0] + "\" введена неверно.";
            throw(ecxeption);
        }
    }

   this->setMarksName(marks);
}

void Template::readTemplate(QString name){

     QFile file(name);
     QList<QString> text;
    if(file.exists()&&file.open(QIODevice::ReadOnly)){
        //читаем построчно
       while(!file.atEnd()){
           text.append(file.readLine());
       }
        //убираем символ перевода строки
       for(int i = 0; i < text.size() - 1; i++){
            text[i].remove(text[i].length()-2, 2);
       }

       file.close();
       this->setTextTemplate(text);
       this->getMarksFromText();
    }
     else {
        QString ecxeption = "File with template not exist";
        throw(ecxeption);
    }
}
void Template::generateEmails(Csv data){
    QStringList inputTemplate = this->getTextTemplate();
   QList<QStringList> result;
   QStringList currentEmail;
   QStringList namesOfMarks = data.getSeparatedData().first();

   QList<QStringList> dataForEmails;
   QStringList marksFromTemplate = getMarksName();
   QStringList list;
   if(marksFromTemplate.size()>namesOfMarks.size()){
       QString exception = "Недостаточно данных для заполнения.";
       throw exception;
   }
   //для каждого письма
   for(int i = 0; i < data.getNumberOfTemplates(); i++){
       if(marksFromTemplate.size()>data.getSeparatedData().take(i+1).size()){
           QString exception = "Недостаточно данных для заполнения.";
           throw exception;
       }
       //для каждой метки из шаблона
       for(int j = 0; j < marksFromTemplate.size(); j++){
           //для каждой метки из входных данных
           for(int f = 0; f < namesOfMarks.size(); f++){
               //если названия меток равны
               if(marksFromTemplate[j].toUpper()==namesOfMarks[f].toUpper()){
                   list.append(data.getSeparatedData().take(i+1).takeAt(f));
                   break;
               }
           }
       }
       dataForEmails.append(list);
       list.clear();
   }

   for(int i = 0; i < data.getNumberOfTemplates(); i++){
        int f = 0;
        currentEmail = this->getTextTemplate();
           QRegExp reg("[^\\\\]#([A-Za-z_]+)#");
       for(int j = 0; j < inputTemplate.size(); j++){
           int pos = 0;
           while((pos = reg.indexIn(currentEmail[j], pos)) != -1){
             currentEmail[j].replace(pos+1, reg.matchedLength() - 1, dataForEmails[i][f]);
              f++;
           }
        }
        result.append(currentEmail);
        currentEmail.clear();
   }
   this->setEmails(result);
}

void Template::uploadToFiles(QString pathName){
	if(!pathName.isEmpty()){
    QList<QStringList> emails = this->getEmails();
   for(int i = 0; i < emails.size(); i++){
        QFile file(pathName + "\\email_№"+QString::number(i+1)+".txt");
         if (file.open(QIODevice::WriteOnly)) {
           QTextStream out(&file);
           for(int j = 0; j < emails[i].size(); j++){
                 out << emails[i][j] << endl;
           }
         }
          file.close();
		}
	} else {
		QString exception = "A path is not specified";
		throw(exception);
	}

}
