/*!
 * \file csv.h
 * \brief Заголовочный файл с описанием класса Csv
 */
#ifndef CSV_H
#define CSV_H
#include "QMap"
#include "QList"


/*!
 * \class Csv
 * \brief Класс для чтения и обработки входных данных
 */
class Csv
{
public:
    /// Конструктор класса Csv
    Csv();

    /*!
     * \brief Метод set для поля с количеством будущих писем
     */
    void setNumberOfTemplates(int number);
    /*!
     * \brief Метод get для поля с количеством будущих писем
     */
    int getNumberOfTemplates();

    /*!
     * \brief Метод set для поля с входными данными с разделителем
     */
    void setData( QStringList data);
    /*!
     * \brief Метод get для поля с входными данными с разделителем
     */
    QStringList getData();

    /*!
     * \brief Метод set для поля с разделенными входными данными
     */
    void setSeparatedData(QMap<int, QStringList> sepData);
    /*!
     * \brief Метод get для поля с разделенными входными данными
     */
    QMap<int, QStringList> getSeparatedData();

    /*!
     * \brief Метод для выделения из строки с входными данными подстроки
     */
    void processData();

    /*!
     * \brief Метод для чтения файла с входными данными и заполнения объекта Csv
     * \param [in] name Имя файла с входными данными
     */
    void readData(QString name);

private:
    int numberOfTemplates; ///< количество будущих писем
    QStringList data; ///< входные данные через с разделителем
    QMap<int, QStringList> separatedData; ///< разделенные входные данные
};

#endif // CSV_H
